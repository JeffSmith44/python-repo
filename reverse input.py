user_input = input()
reverse_input = ''
while user_input != 'done' and user_input != 'Done' and user_input != 'd':
    for character in user_input:
        reverse_input += character

    for character in reversed(reverse_input):
        print(character, end='')

    user_input = input()
    reverse_input = ''
