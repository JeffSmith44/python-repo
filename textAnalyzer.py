def get_num_of_characters(phrase):
    num = 0
    for number_of_chars in phrase:
        num = num + 1
    return num


if __name__ == '__main__':
    phrase = input('Enter a sentence or phrase:')
    print(f'\nYou entered: {phrase}')
    print()
    num2 = get_num_of_characters(phrase)
    print('Number of characters:',num2)
    print('String with no whitespace: ', phrase) 
    ##print(num2)
    