user_input= input('enter numbers: ')
lines = user_input.split(',')

# This line uses a construct called a list comprehension, introduced elsewhere,
# to convert the input string into a two-dimensional list.
# Ex: 1 2, 2 4 is converted to [ [1, 2], [2, 4] ]

mult_table = [[int(num) for num in line.split()] for line in lines]

for i in range(len(mult_table)):
    
    for k in range(len(mult_table)):
        print(mult_table[i][k], end=' ')
        if k < (len(mult_table) - 1):
            print(' | ', end='')
   
    print()
