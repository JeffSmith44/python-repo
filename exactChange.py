
def exact_change(input_val):
    quarters = int(input_val/ 25)
    quarter_remainder =  input_val % 25
    dimes = int( quarter_remainder / 10)
    dime_remainder = quarter_remainder % 10
    nickels = int(dime_remainder / 5)
    pennies = dime_remainder % 5
    
    if pennies == 1:
        print(f'{pennies} penny')
    elif pennies != 0:
        print(f'{pennies} pennies')
        
    if nickels == 1:
        print(f'{nickels} nickel')
    elif nickels != 0:
        print(f'{nickels} nickels')
        
    if dimes == 1:															
        print(f'{dimes} dime') 
    elif dimes != 0:
        print(f'{dimes} dimes') 
           
    if quarters == 1:
            print(f'{quarters} quarter')
    elif quarters != 0:
            print(f'{quarters} quarters')  
            
       
    return(pennies, nickels, dimes, quarters)
    

if __name__ == '__main__': 
    input_val = int(input('Enter total number of change:'))   
    if input_val == 0:
        print('no change')
    else:
        num_pennies, num_nickels, num_dimes, num_quarters = exact_change(input_val)
            