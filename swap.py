

# Define your function here.
def swap_values(user_val1, user_val2, user_val3, user_val4):
    new1 = user_val2
    new2 = user_val1
    new3 = user_val4
    new4 = user_val3
    return new1, new2, new3, new4
  
if __name__ == '__main__': 
    
    user_val1 = input('Enter value #1: ')
    user_val2 = input('Enter value #2: ')
    user_val3 = input('Enter value #3: ')
    user_val4 = input('Enter value #4: ')
        
    new1, new2, new3, new4 = swap_values(user_val1, user_val2, user_val3, user_val4)
    print(new1, new2, new3, new4)