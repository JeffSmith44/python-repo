## Author: Jeff Smith
## 11/11/2023

#A dictionary for the simplified dragon text game
#The dictionary links a room to other rooms.
rooms = {
        'Home Planet': {'east': 'Training Planet','north': 'Starport', 'south': 'Star Destroyer', 'west': 'Nearby Village'},
        'Nearby Village': {'east': 'Home Planet', 'Item': 'jedi-master'},
        'Starport': {'south': 'Home Planet', 'east': 'Armory', 'Item': 'spaceship'},
        'Armory': {'west': 'Starport', 'Item': 'lightsaber'},
        'Star Destroyer': {'north': 'Home Planet', 'east': 'Weapons Factory', 'Item': 'princess'},
        'Weapons Factory': {'west': 'Star Destroyer', 'Item': 'design-plans'},
        'Training Planet': {'west': 'Home Planet', 'north': 'Death Star', 'Item': 'jedi-training'},
        'Death Star': {'south': 'Training Planet', 'Villian': 'Darth Vader'}
    }

#Welcome player to the game and provide instructions
print('Welcome to the Simplified Dragon Text game!\n')      
print('\nEnter the following commands to move from room to room, or to exit the game.\n')
print('\tEnter "go East" to go east')          
print('\tEnter "go West" to go west') 
print('\tEnter "go North" to go north') 
print('\tEnter "go South" to go south') 

print('\tEnter the exact name of the item to place the item in your inventory')

print('\tEnter "exit" to move to "exit room" to end the game.')


#initial some key values
user_input = ''
inventory = []   
current_room = 'Home Planet'

print(f'\nYou are currently in the *** {current_room} ***')
print(f'          Inventory: {inventory}')


# loop to execute the Star Wars adveture text game
while True:
    
    if "Item" in rooms[current_room].keys():
        room_item = rooms[current_room]["Item"]

        if room_item not in inventory:
            print(f'\nThere is a *** {room_item} *** to aid you on your quest\n')
       

    if "Villian" in rooms[current_room].keys():

        if len(inventory) < 6:
            print(f'\n\nYou lose an epic battle with {rooms[current_room]["Villian"]}!  \n            ==== You lose! ====')
            user_input = 'exit'
        
        else:
            print(f'\n\nYou defeated {rooms[current_room]["Villian"]} \n\n ==== You win! ==== \n\n ==== You Win! ====')
        
        break


    user_input = input('\n\nEnter your next move: ')  #allows user to enter commands

            
    if user_input == 'exit':  #allow user to exit
        current_room = 'Exit' 
        print('\nYou have exited the game.') 
        break
    else:

        move = user_input.split(' ')

        action = move[0].lower()
        
        if len(move) > 1:
            room_item = move[1:]
            direction = move[1].lower()

            room_item = ' '.join(room_item).lower()

        if action == 'go':  #executes logic to move from room to room
            
            try:
                current_room = rooms[current_room][direction]  #check for a valid move direction for the current room
                print(f'\n\nYou are now in the *** {current_room} ***')
                print(f'\n        Inventory: {inventory}')
            # print('-----------------------------\n')

            except:
                print('\nYou can not go that way.')
        
        elif action == 'get':   #executes logic to get item to add the the user's inventory
            try:
                if room_item == rooms[current_room]['Item']:
                    
                    if room_item not in inventory:

                        inventory.append(rooms[current_room]['Item'])
                        print(f'\n\nYou have added *** {room_item} *** to your inventory')
                        print(f'\n        Inventory: {inventory}')
                    
                    else:
                        print(f'You already have that item in your inventory')
                else:
                    print('Can\'t the item you requested. Hint: check possible spelling error')

            except:
                    print('Can\'t the item you requested. Hint: check possible spelling error')
        else:
            print('invalid command')

    

  

    