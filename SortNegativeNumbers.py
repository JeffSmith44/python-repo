user_input = input('Enter numbers:')

tokens = user_input.split()  # Split into separate strings

# Convert strings to integers
nums = []
for token in tokens:
    nums.append(int(token))

# Determine maximum even number
neg_num = []
for num in nums:
    if (num < 0):
        # First even number found
        neg_num.append(num)

sorted_neg_num = sorted(neg_num)

for neg in reversed(sorted_neg_num):
    print(neg, end=' ')
# reverse_neg = reversed(sorted_neg_num)
# for i in range(len(reverse_neg)):
#     print(f'{reverse_neg[i]}', end='')