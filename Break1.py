# Program: Break example
# test_python_install.py
# Author: Jeff Smith
# CIS156 at CGCC

# Demo of break example

import math

stop = int(input('enter input:'))
result = 0
for a in range(3):
    print(a, end=': ')
    for b in range(4):
        result += a + b
        if result > stop:
            print('-', end=' ')
            continue
        print(result, end=' ')
    print()	
Input
11
Output
1
2