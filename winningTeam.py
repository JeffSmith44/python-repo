class Team:
    def __init__(self):
        self.name = 'none'
        self.wins = 0
        self.losses = 0
        self.win_percentage = 0
        self.record = 0
        

    # TODO: Define get_win_percentage()
    def get_win_percentage(self, user_wins, user_losses):
        self.wins = user_wins
        self.losses = user_losses
        self.win_percentage = self.wins / (self.wins + self.losses)
        if team.win_percentage < .5:
            self.record = 'losing'
        else:
            self.record = 'winning'
    
    # TODO: Define print_info() method to output model_year, purchase_price, and current_value
    def print_standing(self):
        print(f'Win percentage: {self.win_percentage:.2f}')

        if self.record == 'losing':
            print(f'Team Angels has a {self.record} average.')
        else:
            print(f'Congratulations, Team {self.name} has a {self.record} average!')

    
if __name__ == "__main__":
    team = Team()
   
    user_name = input()
    user_wins = int(input())
    user_losses = int(input())
    
    team.name = user_name
    team.wins = user_wins
    team.losses = user_losses

    team.get_win_percentage(user_wins, user_losses)
    team.print_standing()
