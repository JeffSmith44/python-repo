    # elif num_days == 0 and num_pennies != 1:
    #     pass
    # else:
    #     #print(f'number of days {num_days} {num_pennies}')
    #     total_pennies = double_pennies((num_pennies * 2), (num_days - 1))
# Returns number of pennies if pennies are doubled num_days times
def double_pennies(num_pennies, num_days):

    if  num_days == 0 and num_pennies == 1:
        total_pennies = 1
    elif num_days == 0 and num_pennies != 1:
        pass
    else:
         total_pennies = double_pennies((num_pennies * 2), (num_days - 1))

    return total_pennies

# Program computes pennies if you have 1 penny today,
# 2 pennies after one day, 4 after two days, and so on
starting_pennies = int(input(f'Enter number of pennies: '))
user_days = int(input(f'Enter number of days: '))

print(f'Number of pennies after {user_days} days: ', end="")
print(double_pennies(starting_pennies, user_days))