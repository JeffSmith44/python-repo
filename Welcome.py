# Program: Python installation test program
# test_python_install.py
# Author: David Baker
# CIS156 at CGCC

# Quick test program to check the initial Python installation

import math

# Substitute my name for Mr. Baker's name on the next line
student_name = 'Jeff'

print("The value of pi supplied by the math module is", math.pi)

print("Look like the Python install for", student_name, "works correctly.")