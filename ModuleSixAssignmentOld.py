## Author: Jeff Smith
## 11/9/2023

#A dictionary for the simplified dragon text game
#The dictionary links a room to other rooms.
rooms = {
        'Great Hall': {'South': 'Bedroom'},
        'Bedroom': {'North': 'Great Hall', 'East': 'Cellar', 'Item': 'watch'},
        'Cellar': {'West': 'Bedroom', 'East': 'Final', 'Item': 'gun'},
        'Final': {'West': 'Cellar', 'Villian': "Darth Vader"}
    }

 #Welcome player to the game and provide instructions
print('Welcome to the Simplified Dragon Text game!\n')      
print('\nEnter the following commands to move from room to room, or to exit the game.\n')
print('\tEnter "East" to go east')          
print('\tEnter "West" to go west') 
print('\tEnter "North" to go north') 
print('\tEnter "South" to go south') 

print('\tEnter the exact name of the item to place the item in your inventory')

print('\tEnter "exit" to move to "exit room" to end the game.')

#initial some key values
user_input = ''
inventory = []   
current_room = 'Great Hall'

print(f'\nYou are currently in the *** {current_room} ***')
print(f'Inventory: {inventory}')
print('-----------------------------')

while user_input != 'exit':
    
    user_input = input('\nEnter your next move: ')

   


    if current_room in rooms: 
        try:
            current_room = rooms[current_room][user_input]  #check for a valid move direction for the current room
            print(f'\nYou are now in the *** {current_room} ***')
            print(f'\nInventory: {inventory}')
            print('-----------------------------')

        except:
            print('\nYou can not go that way.')
            
    if "Item" in rooms[current_room].keys():
        room_item = rooms[current_room]["Item"]

        if room_item not in inventory:
            print(f'There is a "{room_item}" to aid you on your quest')
            inventory.append(rooms[current_room]['Item'])
        else:
            print('you already have this item')
    

    if "Villian" in rooms[current_room].keys():

        if len(inventory) < 6:
            print(f'You lose an epic battle with {rooms[current_room]["Villian"]}!  You lose!')
            user_input = 'exit'
        else:
            print(f'You defeat {rooms[current_room]["Villian"]}')
            break
    
    


   
    
   



    if user_input == 'exit':  #allow user to exit
        current_room = 'Exit' 
        print('\nYou have exited the game.') 


    