

#A dictionary for the simplified dragon text game
#The dictionary links a room to other rooms.
rooms = {
        'Great Hall': {'South': 'Bedroom'},
        'Bedroom': {'North': 'Great Hall', 'East': 'Cellar'},
        'Cellar': {'West': 'Bedroom'}
    }

 #Welcome player to the game and provide instructions
print('Welcome to the Simplified Dragon Text game!\n')      
print('\nEnter the following commands to move from room to room, or to exit the game.\n')
print('\tEnter "East" to go east')          
print('\tEnter "West" to go west') 
print('\tEnter "North" to go north') 
print('\tEnter "South" to go south') 
print('\tEnter "exit" to move to "exit room" to end the game.')

#initial some key values
user_input = ''
current_room = 'Great Hall'

print(f'\nYou are currently in the *** {current_room} ***.')

while user_input != 'exit':
    
    user_input = input('\nEnter a command to move to the next room: ')
    
    if user_input == 'exit':  #allow user to exit
        current_room = 'Exit' 
        print('\nYou have exited the game.') 

    elif current_room in rooms: 
        try:
            current_room = rooms[current_room][user_input]  #check for a valid move direction for the current room
            print(f'\nYou are now in the *** {current_room} ***.')

        except:
            print('\nYou can not go that way.')


    