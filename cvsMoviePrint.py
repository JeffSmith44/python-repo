import csv

filename = input()
with open(filename, 'r') as csvfile:
    pile_of_words_reader = csv.reader(csvfile, delimiter=',')

    row_num = 1
    for row in pile_of_words_reader:
        # your_dictionary = build_dictionary(row)
        if row_num == 1: 
            my_str = row[1]
            my_str2 = my_str[0:44]
            print(f'{my_str2:<45}|{row[2]:>6} | {row[0]}',end='')
            prev_value = row[1]
        elif prev_value == row[1]:
             print(f' {row[0]}', end='')
        else:
             my_str = row[1]
             my_str2 = my_str[0:44]
             print(f'\n{my_str2:<45}|{row[2]:>6} | {row[0]}', end='')
             prev_value = row[1]

        row_num += 1
    
    print()

