# Program: Python installation test program
# test_python_install.py
# Author: David Baker
# CIS156 at CGCC

# Quick test program to check the initial Python installation

import math
total_change = int(input('enter change'))
	
dollars = int(total_change / 100)
dollars_remainder = total_change % 100
quarters = int(dollars_remainder / 25)
quarter_remainder = dollars_remainder % 25
dimes = int( quarter_remainder / 10)
dime_remainder = quarter_remainder % 10
nickels = int(dime_remainder / 5)
pennies = dime_remainder % 5

print()	
if total_change == 0:
    print('No change') 
else:
    if dollars == 1:
        print(f' {dollars} dollar')
    elif dollars != 0:
            print(f' {dollars} dollars')

if quarters == 1:
        print(f' {quarters} quarter')
elif quarters != 0:
        print(f' {quarters} quarters')  

if dimes == 1:															
    print(f' {dimes} dime') 
elif dimes != 0:
    print(f' {dimes} dimes') 
       
if nickels == 1:              
    print(f' {nickels} nickel')
elif nickels != 0:
    print(f' {nickels} nickels') 


if pennies == 1:   
    print(f' {pennies} penny')
elif pennies != 0:
    print(f' {pennies} pennies')   
    