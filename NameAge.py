# Program: NameAge
# Objective: Program will ask user their age and will tell them the year they were born
# Author: Jeff Smith
# CIS156 at CGCC
# Assignment: Problem Set 1: IDE (VSCODE)


import math

import datetime
today = datetime.datetime.now()

Name = input('what is your name?:')
Age = int(input('\nHow old are you?:'))
currentYear = today.year
yearBorn = currentYear - Age

print(f'\nHello {Name}! You were born in {yearBorn}.\n')


#**************************************************************************************************
#   what is your name?:Jeff
# 
#   How old are you?:57
#
#   Hello Jeff! You were born in 1966.
#
#***************************************************************************************************