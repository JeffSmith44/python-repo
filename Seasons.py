input_month = input('Enter Month')
input_day = int(input('Enter day'))

months_of_year = {'January': 31, 'February': 28, 'March': 31, 'April': 30, 'May': 31, 'June': 30, 'July': 31, 'August': 31, 'September': 30, 'October': 31, 'November': 30, 'December': 31}

summer = {'June': 21, 'July': 31 , 'August': 31, 'September': 21}
autumn = {'September': 22, 'October': 31, 'November': 30, 'December': 20}
winter = {'December': 21, 'January': 31, 'February': 28, 'March': 19}
spring = {'March': 20, 'April': 30, 'May': 31, 'June': 20}

if (input_month in months_of_year) and (0 < input_day <= months_of_year[input_month]):
    if (input_month in summer) and (input_month == 'June') and (input_day >= summer[input_month]):
        print('Summer')
    elif (input_month in summer) and (input_day <= summer[input_month]) and (input_month != 'June'):
        print('Summer')

    if (input_month in autumn) and (input_month == 'September') and (input_day >= autumn[input_month]):
        print('Autumn')
    elif (input_month in autumn) and (input_day <= autumn[input_month]) and (input_month != 'September'):
        print(f'Autumn')

    if (input_month in winter) and (input_month == 'December') and (input_day >= winter[input_month]):
        print('Winter')
    elif (input_month in winter) and (input_day <= winter[input_month])and (input_month != 'December'):
        print('Winter')

    if (input_month in spring) and (input_month == 'March') and(input_day >= spring[input_month]):
        print('Spring')
    elif (input_month in spring) and (input_day <= spring[input_month])and (input_month != 'March'):
        print('Spring')
else:
    print('Invalid input')