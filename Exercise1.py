

stop = 17
total = 0
for number in [7, 4, 6, 4, 5, 3]:
    print(number, end=' ')
    total += number
    if total > stop:
        print('$')
        break
else:
    print(f'/ {total}')
print('done')