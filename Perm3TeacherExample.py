def print_all_permutations(permList, nameList):
    # Helper function to recursively generate and print all permutations
    def helper(permList, nameList):
        if not nameList:
            # If the nameList is empty, all elements are in permList
            permutation_str = ' '.join(permList)
            print(permutation_str)
            return
        for name in nameList:
            # Append the next name to permList and recursively generate permutations
            permList.append(name)
            helper(permList, [n for n in nameList if n != name])
            # Remove the last added name to backtrack and generate other permutations
            permList.pop()

    helper(permList, nameList)

if __name__ == "__main__":
    nameList = input("Enter a list of words: ").split(' ')
    permList = []  # Initialize as an empty list
    print_all_permutations(permList, nameList)