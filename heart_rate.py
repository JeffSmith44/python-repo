def get_age():
    age = int(input())
    # TODO: Raise exception for invalid ages
    if 17 < age < 76:
        print(f'Invalid age.\nCould not calculate heart rate info.')
    else:
      
        print(f'Invalid age.\nCould not calculate heart rate info.')
        raise ValueError
    return age

# TODO: Complete fat_burning_heart_rate() function
def fat_burning_heart_rate(age):
    heart_rate = .70 * (220 - age)

    return heart_rate

if __name__ == "__main__":
    # TODO: Modify to call get_age() and fat_burning_heart_rate()
    #       and handle the exception
    age = get_age()
    heart_rate = fat_burning_heart_rate(age)
    print(f'Fat burning heart rate for a {age} year-old: {heart_rate} bpm')