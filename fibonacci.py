# TODO: Write recursive fibonacci() function

def fibonacci(start_num):
     
    if start_num < 0:
        return -1
    elif start_num == 0:
        return 0
    elif start_num == 1 or start_num == 2:
        return 1
    else:
        sum1 = fibonacci(start_num-1) 
        sum2 = fibonacci(start_num-2)
        sum_total = sum1 + sum2

        return sum_total


if __name__ == "__main__":
   start_num = int(input(f' Enter Fibonacci#:'))
   print(f'fibonacci({start_num}) is {fibonacci(start_num)}')
