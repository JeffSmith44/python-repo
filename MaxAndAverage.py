user_input = input('Enter numbers:')

tokens = user_input.split()  # Split into separate strings

# Convert strings to integers
nums = []
for token in tokens:
    nums.append(float((token)))

average = float(sum(nums) / len(nums))
maxx = max(nums)

print(f' Max: {maxx:.2f} Average: {average:.2f}')