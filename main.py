import string

### DO NOT MODIFY THIS FUNCTION ###
def load_words(file_name):
    '''
    file_name (string): the name of the file containing 
    the list of words to load    
    
    Returns: a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    '''
    print('Loading word list from file...')
    # inFile: file
    in_file = open(file_name, 'r')
    # line: string
    line = in_file.readline()
    # word_list: list of strings
    word_list = line.split()
    print('  ', len(word_list), 'words loaded.')
    in_file.close()
    return word_list

### DO NOT MODIFY THIS FUNCTION ###
def is_word(word_list, word):
    '''
    Determines if word is a valid word, ignoring
    capitalization and punctuation

    word_list (list): list of words in the dictionary.
    word (string): a possible word.
    
    Returns: True if word is in word_list, False otherwise

    Example:
    >>> is_word(word_list, 'bat') returns
    True
    >>> is_word(word_list, 'asdf') returns
    False
    '''
    word = word.lower()
    word = word.strip(" !@#$%^&*()-_+={}[]|\:;'<>?,./\"")
    return word in word_list

### DO NOT MODIFY THIS FUNCTION ###
def get_story_string():
    """
    Returns: a joke in encrypted text.
    """
    f = open("story.txt", "r")
    story = str(f.read())
    f.close()
    return story

WORDLIST_FILENAME = 'words.txt'

class Message(object):
    ### DO NOT MODIFY THIS METHOD ###
    def __init__(self, text):
        '''
        Initializes a Message object
                
        text (string): the message's text

        a Message object has two attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words
        '''
        self.message_text = text
        self.valid_words = load_words(WORDLIST_FILENAME)

    ### DO NOT MODIFY THIS METHOD ###
    def get_message_text(self):
        '''
        Used to safely access self.message_text outside of the class
        
        Returns: self.message_text
        '''
        return self.message_text

    ### DO NOT MODIFY THIS METHOD ###
    def get_valid_words(self):
        '''
        Used to safely access a copy of self.valid_words outside of the class
        
        Returns: a COPY of self.valid_words
        '''
        return self.valid_words[:]
        
    def build_shift_dict(self, shift): 
        '''
        Creates a dictionary that can be used to apply a cipher to a letter.
        The dictionary maps every uppercase and lowercase letter to a
        character shifted down the alphabet by the input shift. The dictionary
        should have 52 keys of all the uppercase letters and all the lowercase
        letters only.        
        
        shift (integer): the amount by which to shift every letter of the 
        alphabet. 0 <= shift < 26

        Returns: a dictionary mapping a letter (string) to 
                 another letter (string). 
        '''
              
        shift_dict = {}
        lower_letters = string.ascii_lowercase #a b c d e 
        upper_letters = string.ascii_uppercase
        lower_size = len(lower_letters) #26
        for ch in lower_letters:         #wrap around if the distance from 'a' is greater than 26 hint: use modulo ops 26%26 = 0 27 %26 = 1
            x = lower_letters.index(ch)
            if x < (len(lower_letters) - shift):
                shift_dict[ch] = chr(ord(ch) + shift)
            else:
                p = chr((ord(ch)- (26 - shift)))     
                shift_dict[ch] = p
              
        for ch in upper_letters:         #wrap around if the distance from 'a' is greater than 26 hint: use modulo ops 26%26 = 0 27 %26 = 1
            x = upper_letters.index(ch)
            if x < (len(upper_letters) - shift):
                shift_dict[ch] = chr(ord(ch) + shift)
            else:
                p = chr((ord(ch)- (26 - shift)))
                shift_dict[ch] = p
                
        return shift_dict
     
        

    def apply_shift(self, shift):   #Encrypts user input based on the given shift value
        '''
        Applies the Caesar Cipher to self.message_text with the input shift.
        Creates a new string that is self.message_text shifted down the
        alphabet by some number of characters determined by the input shift        
        
        shift (integer): the shift with which to encrypt the message.
        0 <= shift < 26

        Returns: the message text (string) in which every character is shifted
             down the alphabet by the input shift
        '''
      
        self.message_text1 = ''

        for ch in self.message_text:
           self.message_text1 = self.message_text1 + self.encryping_dic[ch]
            
        return self.message_text1
        
     
        

class PlaintextMessage(Message):
    def __init__(self, text, shift):
        '''
        Initializes a PlaintextMessage object        
        
        text (string): the message's text
        shift (integer): the shift associated with this message

        A PlaintextMessage object inherits from Message and has five attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words)
            self.shift (integer, determined by input shift)
            self.encrypting_dict (dictionary, built using shift)
            self.message_text_encrypted (string, created using shift)

        Hint: consider using the parent class constructor so less 
        code is repeated
        '''
        self.message_text = text
        self.cipherText = ''
            
       
        self.shift = shift
        self.encryping_dic = super().build_shift_dict(self.shift)
 

    def get_shift(self):
        '''
        Used to safely access self.shift outside of the class
        
        Returns: self.shift
        '''
        #TODO
        
        return self.shift

    def get_encrypting_dict(self):
        '''
        Used to safely access a copy self.encrypting_dict outside of the class
        
        Returns: a COPY of self.encrypting_dict
        '''
        #TODO
        
        return self.encrypting_dict

    def get_message_text_encrypted(self):
        '''
        Used to safely access self.message_text_encrypted outside of the class
        
        Returns: self.message_text_encrypted
        '''
        self.message_text_encrypted = super().apply_shift(self.shift)
       
                
        return self.message_text_encrypted

    def change_shift(self, shift):
        '''
        Changes self.shift of the PlaintextMessage and updates other 
        attributes determined by shift (ie. self.encrypting_dict and 
        message_text_encrypted).
        
        shift (integer): the new shift that should be associated with this message.
        0 <= shift < 26

        Returns: nothing
        '''
       
        
        ciphertext.build_shift_dict(self.shift)
        self.shift += 1
        



class CiphertextMessage(Message):
    def __init__(self, text):
        '''
        Initializes a CiphertextMessage object
                
        text (string): the message's text

        a CiphertextMessage object has two attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words)
        '''
        
        self.message_text = text
        self.valid_words = load_words(WORDLIST_FILENAME)
        self.shift = 0
        self.encryping_dic = super().build_shift_dict(self.shift)
        
       
        

    def decrypt_message(self):
        '''
        Decrypt self.message_text by trying every possible shift value
        and find the "best" one. We will define "best" as the shift that
        creates the maximum number of real words when we use apply_shift(shift)
        on the message text. If s is the original shift value used to encrypt
        the message, then we would expect 26 - s to be the best shift value 
        for decrypting it.

        Note: if multiple shifts are  equally good such that they all create 
        the maximum number of you may choose any of those shifts (and their
        corresponding decrypted messages) to return

        Returns: a tuple of the best shift value used to decrypt the message
        and the decrypted message text using that shift value
        '''
       
        self.letter1 = ''
              
        self.shift = 0
        best_shift, best_plaintext = 0, ""
        max_validword = 0
        MAX_SHIFT = 26
        valid = 0
        pt = PlaintextMessage(self.message_text, best_shift)

        for shift in range(0,MAX_SHIFT):    #for loop cycles through all 26 possible iterations of the encryption dictionary
            pt.change_shift(-shift)

            cipherText = ''

            self.encryping_dic = super().build_shift_dict(shift)    # builds an encryption dictionary based for each shift value
            
            for letter in self.message_text:    #for loop decrypts the input message and stores encrypted results in cipherText
                if letter != ' ':
                    for key, value in self.encryping_dic.items():   #uses the encryption dictionary to decrypt each word
                        if letter == value:
                            letter1 = key
                            cipherText = cipherText + letter1
                else:
                    cipherText = cipherText + ' '

            plain_txt = cipherText
            
            for word in plain_txt.split(' '):   #checks each word against a list of valid words to determine the number of valid words decryted in bove for loop
          
                if is_word(self.get_valid_words(), word):
                    valid += 1

            if valid > max_validword:   #keeps track of the best results for the 26 iterations of the decryption process
                best_shift = shift
                max_validword = valid
                best_plaintext = cipherText
                valid = 0
      
        return best_shift, best_plaintext
    

if __name__ == "__main__":
     print('\n')
     # Example test case (PlaintextMessage)
     plaintext = PlaintextMessage('hello', 2)
     print('Expected Output: jgnnq')
     print('Actual Output:', plaintext.get_message_text_encrypted())
     print('\n')

     #Example test case (CiphertextMessage)
     ciphertext = CiphertextMessage('jgnnq')
     print('Expected Output:', (2, 'hello'))
     print('Actual Output:', ciphertext.decrypt_message())
     print('\n')
    
    # #Example test case (CiphertextMessage)
     ciphertext = CiphertextMessage(get_story_string())
     print('Actual Output:', ciphertext.decrypt_message())