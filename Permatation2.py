#num_cities = 3
#nameList = []

def print_all_permutations(permList, name_list):
    if len(permList) == len(nameList):  #Base case:
 
        for i in range(len(permList)):
            print(f'{nameList[permList[i]]}', end='')
            if i < (len(permList) - 1):
                print(', ', end='')
        print(f'')
        
    else:  # Recursive case: 
        for i in range(len(name_list)):
            city = name_list[i]
            name_list.pop(i)
            permList.append(city)

            print_all_permutations(permList, name_list)

            name_list.insert(i, city)
            permList.pop()


if __name__ == "__main__": 
    nameList = input('Enter names:').split(' ')
    permList = []
    name_list = [] 
    for i in range(len(nameList)):
        name_list.append(i) 

    
   
    print_all_permutations(permList, name_list)
