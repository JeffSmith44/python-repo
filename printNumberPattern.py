
def print_num_pattern(num1, num2):


    if num1 < 0: 
        print(num1, end=" ") 
        return
    else:
        print(num1, end=" ")
        print_num_pattern(num1 - num2, num2)
        print(num1, end=" ") 
        orig_val = num1 + num2 - (num1 % num2)
        if num1 < orig_val: # base case
            return
        print_num_pattern(num1 + num2, num2) #recursively


if __name__ == "__main__":
    num1 = int(input('enter start number'))
    num2 = int(input('enter change number'))
    print_num_pattern(num1, num2)